## Projext structures

- src (source)
  -- config -> sequelize config
  -- migrations ->
  -- models -> table's models
  -- seeders -> data dummy creation
  -- app.js -> app's middleware json
  --

## model

npx sequelize-cli model:generate --name user_game --attributes usernameId:string,password:string

## create customer model

```
npx sequelize-cli model:generate --name user_game_biodata --attributes fullName:string,nationality:string,age:integer,usernameId:string
```

## create order model

```
npx sequelize-cli model:generate --name user_game_history --attributes playedAt:date,usernameID:string
```

## migration

```
npx sequelize-cli  db:create
```

```
npx sequelize-cli db:migrate
```

## seeding

```
npx sequelize-cli seed:generate --name initial-sale

```

npx sequelize-cli db:seed:all

```
npx sequelize-cli db:seed:undo
```

# RESTful API Authentication

- `POST` /auth/login

Payload

```json
{
  "username": "email@address.com",
  "password": "secretPassword"
}
```

Response

```json
{
  "accessToken": "secretToken",
  "accessTokenExpired": "2022/02/01T22:30:10.000Z",
  "refreshToken": "refreshToken",
  "refreshTokenExpired": "2022/02/01T22:30:10.000Z"
}
```

- `GET` /me

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
{
  ...userData
}
```

- `GET` /users

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
[
  { ...userData },
  { ...userData }
]
```

## Credentials

Username: `porsche.huel@email.com`
Password: `password123`

Username: `julius.oconner@email.com`
Password: `password123456`

Username: `melodee.dooley@email.com`
Password: `password123456789`
