const express = require("express");
const { user_game_history } = require ("../models");
const route = express.Router();

route.post("/history", async (req, res) => {
  const { body } = req;
  const history = await user_game_history.create(body);
  res.json(history);
});

route.get("/history", async (req, res) => {
  const histories = await user_game_history.findAll({
    include: [
      {
        model: user_game,
        attributes: [ 'username', 'email' ]
      }
    ]
  });
  res.json(histories);
});

route.get("/history/:id", async (req, res) => {
  const fHistories = await user_game_history.findOne({
    where: {
      id: req.params.id
    },
    include: [
      {
        model: user_game,
        attributes: [ 'username', 'email' ]
      },
      {
        model: user_game_biodata
      }
    ]
  });
  res.json(fHistories);
});


module.exports = route;