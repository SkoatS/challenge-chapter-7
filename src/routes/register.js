const express = require("express");
const route = express.Router();

const { user_game } = require ("../models");

route.post("/register", async (req, res) => {
  const { body } = req;
  const users = await user_game.create(body);
  res.json(users);
});

// read(get) user_game
route.get("/register", async (req, res) => {
  const users = await user_game.findAll();
  res.json(users);
});

module.exports = route;