const express = require("express");
const { user_game_biodata } = require ("../models");
const route = express.Router();

route.post("/biodata", async (req, res) => {
  const { body } = req;
  const biodatas = await user_game_biodata.create(body);
  res.json(biodatas);
});


route.get("/biodata", async (req, res) => {
  const Fbiodata = await user_game_biodata.findAll({
    include: [
      {
        model: user_game
      },
    ]
  });
  res.json(Fbiodata);
});

route.put("/biodata/:id", async (req, res) => {
  const { body } = req;
  const put_biodata = await user_game_biodata.update(body, {
    where: {
      id: req.params.id
    }
  });
  res.json(put_biodata);
});


module.exports = route;