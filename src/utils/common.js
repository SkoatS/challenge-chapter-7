const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken")

module.exports = {
  generateToken: (data) => {
    return jwt.sign(data, process.env.JWT_SECRET)
  },
  encrypt: (password) => {
    return bcrypt.hashSync(password, 10);
  },
  comparePassword(password, encryptedPassword) {
    return bcrypt.compareSync(password, encryptedPassword)
  }
};
